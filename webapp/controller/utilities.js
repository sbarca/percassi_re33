sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"./utilities"
], function (JSONModel) {
	"use strict";

	// class providing static utility methods to retrieve entity default values.

	return {
		commonModel: new JSONModel({
			rootSite: $.sap.getModulePath("fabbisogni/fabbisogni", "")
		}),
		aMarkers: [],
		aMarkersLatLng: [],
		bCallDivZona: undefined,
		oSearchNuovoGiro: {
			Divisione: "",
			Zona: "",
			GiriOccupati: []
		},
		oSelectedPdc: undefined,
		sSelectedPdcPath: "",

		isNumber: function (n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		},

		description: "",

		adjustForTimeZone: function (date) {
			var timeOffsetInMS = date.getTimezoneOffset() * 60000;
			date.setTime(date.getTime() - timeOffsetInMS);
			return date;
		},

		base64ToArray: function (base64) {
			var binary_string = window.atob(base64);
			var len = binary_string.length;
			var bytes = new window.Uint8Array(len);
			for (var i = 0; i < len; i++) {
				bytes[i] = binary_string.charCodeAt(i);
			}
			return bytes;
		},

		arrayBufferToBase64: function (buffer) {
			var binary = '';
			var bytes = new window.Uint8Array(buffer);
			var len = bytes.byteLength;
			for (var i = 0; i < len; i++) {
				binary += String.fromCharCode(bytes[i]);
			}
			return window.btoa(binary);
		},
		base64ToArrayBuffer: function (base64) {
			var binary_string = window.atob(base64);
			var len = binary_string.length;
			var bytes = new window.Uint8Array(len);
			for (var i = 0; i < len; i++) {
				bytes[i] = binary_string.charCodeAt(i);
			}
			return bytes.buffer;
		},

	};
});