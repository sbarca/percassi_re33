sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"RE33/Percassi_RE33/controller/utilities",
	"RE33/Percassi_RE33/controller/formatter",
	"RE33/Percassi_RE33/controller/ErrorHandler",
	"sap/ui/model/json/JSONModel",
], function (Controller, util, formatter, ErrorHandler, JSONModel) {
	"use strict";

	return Controller.extend("RE33.Percassi_RE33.controller.Main", {
		formatter: formatter,

		onInit: function () {
			var oModel = this.getView().getModel('cds_model');
			oModel.setDefaultBindingMode("TwoWay");
			var oViewModel = new JSONModel({
				editable: false,
				busy: false,
				delay: 0
			});
			this.getView().setModel(oViewModel, 'viewModel');
		},

		_onBeforeRebindTable: function () {
			this.getView().getModel('cds_model').metadataLoaded().then(function () {
				var oModel = this.getView().getModel('cds_model');
				var smarttable = this.getView().byId('idProductsTable');
				var id_excel = this.getView().byId("fileUploader").getValue();

				var path = oModel.createKey('/zcds_re033_main', {
					id_excel: id_excel
				});

				smarttable.setModel(oModel);
				smarttable.bindObject(path);

			}.bind(this));
		},

		_onShowAnomalies: function (oEvent) {
			if (!this._AnomaliesTable) {
				this._AnomaliesTable = sap.ui.xmlfragment(this.getView().createId("AnomaliesTable"),
					"RE33.Percassi_RE33.fragment.LogsTable",
					this
				);
				this.getView().addDependent(this._AnomaliesTable);
			}
			this.getView().byId('AnomaliesTable--AnomaliesTable').getBinding('items').filter([]);
			this._AnomaliesTable.open();
		},

		_onOkLogs: function () {
			this._AnomaliesTable.close();
		},

		_handleEditTable: function () {
			var oViewModel = this.getView().getModel("viewModel");
			var edit = oViewModel.getProperty("/editable");
			if (edit == true) {
				oViewModel.setProperty("/editable", false);
			} else {
				oViewModel.setProperty("/editable", true);
			}

		},

		_onProcessUploadedRecords: function (oEvent) {
			var oViewModel = this.getView().getModel("main_model");
			var condType = this.getView().byId('condTypeS').getSelectedItem().getText();
			var righeSelezionate = this.getView().byId('idProductsTable').getTable().getSelectedItems();
			var id_excel = this.getView().byId("fileUploader").getValue();
			for (var i = 0; i < righeSelezionate.length; i++) {
				var obj = righeSelezionate[i].getBindingContext().getObject();
				if (obj.status == 'E') {
					continue;
				}
				obj['condtype'] = condType;
				obj['id_excel'] = id_excel;
				delete obj.__metadata;
				delete obj.processed;

				var key = oViewModel.createKey("/UploadedExcelSet", {
					id_excel: obj.id_excel,
					id: obj.id
				});
				var that = this;
				this.getView().getModel('viewModel').setProperty("/busy", true);
				oViewModel.update(key, obj, {
					success: function (oData) {
						window.console.log('success');
						that.getView().getModel('viewModel').setProperty("/busy", false);
						that.getView().byId('AnomaliesTable--AnomaliesTable').getBinding('items').filter([]);
						that.getView().byId('idProductsTable').getTable().getBinding('items').filter([]);
					},
					error: function (oData) {
						window.console.log('error');
						// that.getView().byId('AnomaliesTable--AnomaliesTable').getBinding('items').filter([]);
						that.getView().getModel('viewModel').setProperty("/busy", false);
					}
				});
			}
		},

		_onUploadReadFile: function (oEvent) {
			var oViewModel = this.getView().getModel("main_model");
			var condType = this.getView().byId('condTypeS').getSelectedItem().getText();
			var righe = this.getView().byId('idProductsTable').getTable().getItems();
			var id_excel = this.getView().byId("fileUploader").getValue();
			var that = this;
			for (var i = 0; i < righe.length; i++) {
				var obj = righe[i].getBindingContext().getObject();
				obj['condtype'] = condType;
				obj['id_excel'] = id_excel;
				delete obj.__metadata;

				var key = oViewModel.createKey("/CheckRecordsSet", {
					id_excel: obj.id_excel,
					id: obj.id
				});

				oViewModel.update(key, obj, {
					success: function (oData) {
						window.console.log('success');
						that.getView().byId('idProductsTable').getTable().getBinding('items').filter([]);
						that.getView().byId('AnomaliesTable--AnomaliesTable').getBinding('items').filter([]);
					},
					error: function (oData) {
						window.console.log('error');
					}
				});
			}
		},

		_onShowProcessed: function (oEvent) {
			this.getRouter().navTo("savedRecords");
		},
		getRouter: function () {
			return this.getOwnerComponent().getRouter();
		},

		_onUploadExcel: function (oEvent) {
			var fU = this.getView().byId("fileUploader");
			var domRef = fU.getFocusDomRef();
			var oFileList = domRef.files;

			if (!oFileList || oFileList.length === 0) {
				sap.m.MessageToast.show(this._resBundle.getText("selectExcel"));
				return;
			}
			this.uploadFileListToSever(oFileList);
		},

		uploadFileListToSever: function (oFileList) {
			var sGroupId = "UPLOAD";
			var numFileUploaded = 0;
			var header = this.getView().byId('headerCB').getSelected();
			var condType = this.getView().byId('condTypeS').getSelectedItem().getText();
			if (header == true) {
				header = 'X';
			} else {
				header = '';
			}
			this.getView().getModel('main_model').setDeferredGroups([sGroupId]);
			if (!oFileList || oFileList.length === 0) {
				var itemKey = this.getView().getModel('main_model').createKey("/ExcelUploadSet", {
					VRSIO: condType
				});
				this.getView().getModel('main_model').update(itemKey, {
					condition: 'ZPV1',
					chiave: '1',
					header: header,
					name: '',
					base64: ''
				}, {
					success: function (oData) {
						sap.m.MessageToast.show('yuppi');
					},
					error: function () {}
				});
				return;
			} else {
				this.uploadFileList = [];
				var checkLoaded = function (oCurrFile, oReader) {
					if (oReader.result) {
						oCurrFile.status = "loaded";
						oCurrFile.object = oReader.result;
					} else {
						oCurrFile.status = oReader.error;
					}
				};
				for (var i = 0; i < oFileList.length; i++) {
					var oReader = new window.FileReader();
					var oCurrFile = {
						"fileName": oFileList[i].name,
						"status": "loading"
					};
					this.uploadFileList.push(oCurrFile);
					oReader.addEventListener("loadend", jQuery.proxy(checkLoaded, this, oCurrFile, oReader));
					oReader.readAsArrayBuffer(oFileList[i]);
				}
				var j;
				var fnToLaunch = function (array) {
					for (j in array) {
						numFileUploaded++;
						var that = this;
						this.getView().getModel('viewModel').setProperty("/busy", true);
						this.getView().getModel('main_model').create("/ExcelUploadSet", {
							condition: condType,
							chiave: array[j].fileName,
							header: header,
							name: array[j].fileName,
							base64: util.arrayBufferToBase64(array[j].object)
						}, {
							success: function (oData) {
								window.console.log('success');
								that.getView().getModel('cds_model').refresh(true);
								that.getView().getModel('cds_model').resetChanges();

								//that.getView().byId('idProductsTable').getTable().getBinding('items').filter([]);
								that._onBeforeRebindTable();
								that.getView().getModel('viewModel').setProperty("/busy", false);
							},
							error: function () {
								window.console.log('error');
								that.getView().getModel('viewModel').setProperty("/busy", false);
							}
						});
					}

					if (numFileUploaded === 0) {
						sap.ui.core.BusyIndicator.hide();
					}

				}.bind(this);

				var check = function () {
					var aData = this.uploadFileList;

					var bReady = true;
					for (j in aData) {
						if (aData[j].status === "loading") {
							bReady = false;
						}
					}
					if (bReady) {
						fnToLaunch(aData);
						return;
					}
					window.setTimeout(check, 500);

				}.bind(this);

				check();
			}

		},

	});
});