sap.ui.define([
	"sap/ui/base/Object",
	"sap/m/MessageBox"
], function (UI5Object, MessageBox) {
	"use strict";

	return UI5Object.extend("fabbisogni.fabbisogni.controller.ErrorHandler", {

		/**
		 * Handles application errors by automatically attaching to the model events and displaying errors when needed.
		 * @class
		 * @param {sap.ui.core.UIComponent} oComponent reference to the app's component
		 * @public
		 * @alias fabbisogni.fabbisogni.controller.ErrorHandler
		 */
		constructor: function (oComponent) {
			this._oResourceBundle = oComponent.getModel("i18n").getResourceBundle();
			var aModels = oComponent.getMetadata().getManifestEntry("sap.ui5").models;
			this._oComponent = oComponent;
			this._bMessageOpen = false;
			var sErrorText = this._oResourceBundle.getText("errorText");
			$.each(aModels, function (modelName, modelMeta) {
				if (modelMeta.type.indexOf("ODataModel") === -1) {
					return;
				}
				var model = modelName === "" ? this._oComponent.getModel() : this._oComponent.getModel(modelName);
				if (!model) {
					return;
				}
				model.attachMetadataFailed(function (oEvent) {
					var oParams = oEvent.getParameters();
					this._showServiceMessage({
						msg: oParams.response,
						type: "error",
						title: sErrorText
					});
				}, this);
				model.attachRequestFailed(function (oEvent) {
					var oParams = oEvent.getParameters();
					// An entity that was not found in the service is also throwing a 404 error in oData.
					// We already cover this case with a notFound target so we skip it here.
					// A request that cannot be sent to the server is a technical error that we have to handle though
					//if (oParams.response.statusCode !== "404" || (oParams.response.statusCode === 404 && oParams.response.responseText.indexOf("Cannot POST") === 0)) {
					var errStr;
					try {
						var errObj = JSON.parse(oParams.response.responseText);
						errStr = errObj.error.message.value;
					} catch (e) {
						if (oParams && oParams.response && oParams.response.responseText) {
							errStr = oParams.response.responseText;
						}
					}
					if (!errStr) {
						this._showServiceMessage({
							msg: oParams.response,
							type: "error",
							title: sErrorText
						});
					} else {
						this._showServiceMessage({
							msg: errStr,
							type: "error",
							title: sErrorText
						});
					}
					//}
				}.bind(this), this);
				// gestione dei messaggi diversi dall'errore
				model.attachRequestCompleted(function (oEvent) {
					var oParams = oEvent.getParameters();
					try {
						var oMsg = JSON.parse(oParams.response.responseText);
						this._showServiceMessage({
							msg: oMsg.error.message.value,
							type: "error",
							aErrorDetails: oMsg.error.innererror.errordetails
						});
					} catch (e) {}
				}.bind(this), this);
			}.bind(this));
		},

		/**
		 * Shows a {@link sap.m.MessageBox} when a service call has failed.
		 * Only the first error message will be display.
		 * @param {string} sDetails a technical error to be displayed on request
		 * @private
		 */
		_showServiceMessage: function (params) {
			if (this._bMessageOpen) {
				return;
			}
			this._bMessageOpen = true;
			MessageBox[params.type](
				params.msg, {
					styleClass: this._oComponent.getContentDensityClass(),
					actions: [MessageBox.Action.CLOSE],
					onClose: function () {
						this._bMessageOpen = false;
					}.bind(this)
				});
		}

	});

});