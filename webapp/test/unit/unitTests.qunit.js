/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"RE33/Percassi_RE33/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});