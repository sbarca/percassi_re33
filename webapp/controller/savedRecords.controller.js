sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"RE33/Percassi_RE33/controller/Main.controller",
	"RE33/Percassi_RE33/controller/utilities",
	"RE33/Percassi_RE33/controller/formatter",
	"sap/ui/model/json/JSONModel",
], function (Controller, Main, util, formatter, JSONModel) {
	"use strict";

	return Controller.extend("RE33.Percassi_RE33.controller.savedRecords", {
		formatter: formatter,

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("savedRecords").attachPatternMatched(this._onObjectMatched, this);
			this.getOwnerComponent().getModel('cds_model').metadataLoaded().then(this._onMetadataLoaded.bind(this));
		},

		_onObjectMatched: function (oEvent) {

			this.getView().getModel('cds_model').metadataLoaded().then(function () {
				var smarttable = this.getView().byId('idProductsTable');
				smarttable.setModel(this.getView().getModel('cds_model'));
				smarttable.getModel().setProperty('/status', 'S');
			}.bind(this));

			// var smarttable = this.getView().byId('idProductsTable');
			// smarttable.getTable().getBinding('items').filter([]);
			// this.onBeforeRebindTable();

		},

		_onMetadataLoaded: function () {

		}

	});

});